сериализуемый класс: 
	class FuncS
    	{
        public int MyInt { get; set; }
        public string MyString { get; set; }
        public static FuncS Get => new FuncS { MyInt = 1, MyString = "Foo" };


код сериализации: 
	public string ToCsv(object o)
        {
            return string.Join("\n", o.GetType().GetProperties().Select(x => x.Name + "," + x.GetValue(o)));
        }

код десериализации:
	...
	var propertyInfo = deserializedClass.GetType().GetProperty(array[0]);
        propertyInfo.SetValue(deserializedClass, Int32.Parse(array[1]));
	...

количество замеров: 
	1_000_000 итераций
